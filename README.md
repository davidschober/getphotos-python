# README #

Get photos and organize them. 

To use from the command line:
```
$getphotos
```
to install

```
sudo pip install --upgrade git+https://davidschober@bitbucket.org/davidschober/getphotos-python.git
```

Note, this requires Phil Harvey's excellent exiftool. 
