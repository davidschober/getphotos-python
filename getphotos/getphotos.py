#!/usr/bin/env python


import os
import fnmatch
import shutil

#########
##SETUP##
#########

TEMP_DIR = "/Users/Shared/Pictures/temp_photos/"
UNPROC = "/Users/Shared/Pictures/unprocessed/"

BASE_DIR = "/Volumes/"

IGNORE = ["Macintosh HD", "Audio CD"] #files to ignore when searching for photos

EXTENSIONS = "*.HEIC *.heic *.JPG *.jpg *.NEF *.MTS *.MOV *.mov *.RAF *.RW2 *.MP4 *.mp4".split()


def find_cards():
    """check to see if card exists by looking for DCIM"""

    return [os.path.join(BASE_DIR, volume, "DCIM") 
                for volume in os.listdir(BASE_DIR) 
                if os.path.isdir(os.path.join(BASE_DIR, volume, "DCIM"))]
    

def get_photos(card):
    """grab a list of photos from the card"""
    matches = []
    for root, dirs, filenames in os.walk(card):
        for ext in EXTENSIONS:
            for filename in fnmatch.filter(filenames, ext):
                matches.append(os.path.join(root, filename))
    return matches #return a list of files matching filetypes

def get_all_photos(cards):
    """grab all photos an any card"""
    all_photos = []
    for card in cards:
        all_photos.extend(get_photos(card))
    return all_photos
 
def move_files(files, target):
    """move the photos and videos to the temp directory"""
    for f in files:
        shutil.move(f, target)    

def rename_photos(source, target):
    """rename all photos and videos in temp, move them to the unprocessed directory"""
    exif_command_movie = "exiftool -ext MP4 -ext MOV -r '-FileName<CreateDate' -d "+target+"video/%Y.%m.%d-%H.%M.%S%%-c.%%le "+source  
    exif_command = "exiftool -r '-FileName<CreateDate' -d "+target+"%Y-%m-%d/raw/%Y.%m.%d-%H.%M.%S%%-c.%%le "+source
    os.system(exif_command)
    os.system(exif_command_movie)

def main():
    """do the dance"""
    
    #grabbing cards
    cards = find_cards()
    print("found %s DCIM files" %len(cards))

    photos = get_all_photos(cards)
    print("found %s photos" %len(photos))
    print("moving photos to %s" %TEMP_DIR)
    move_files(photos, TEMP_DIR)
    print("renaming all photos and putting in %s" %UNPROC)
    rename_photos(TEMP_DIR, UNPROC)

if __name__ == '__main__':
    main()

