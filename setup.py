from setuptools import setup

setup(name='getphotos',
        version='0.1',
        description='a photo getter',
        url='https://davidschober@bitbucket.org/davidschober/getphotos-python.git',
        author='David Schober',
        author_email='davidschob@gmail.com',
        license='MIT',
        packages=['getphotos'],
        install_requires=[
            '',
            ],
        entry_points = {
            'console_scripts': ['getphotos=getphotos.command_line:main'],
            },
        include_package_data=True,
        zip_safe=False)
